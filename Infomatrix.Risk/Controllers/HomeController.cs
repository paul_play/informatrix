﻿using Infomatrix.Risk.Data;
using Infomatrix.Risk.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Infomatrix.Risk.Controllers
{
    public class HomeController : Controller
    {
        private const decimal WIN_THRESHOLD = 0.6M;

        private IRepository<Settled> _settledRespository;
        private IRepository<Unsettled> _unsettledRespository;

        public HomeController(IRepository<Settled> settledRepository, IRepository<Unsettled> unsettledRepository)
        {
            _settledRespository = settledRepository;
            _unsettledRespository = unsettledRepository;
        }

        public HomeController()
        {
            _settledRespository = new Repository<Settled>(new StreamReader(HostingEnvironment.MapPath("/App_Data/settled.csv")));
            _unsettledRespository = new Repository<Unsettled>(new StreamReader(HostingEnvironment.MapPath("/App_Data/unsettled.csv")));
        }

        public ActionResult Settled()
        {
            IEnumerable<GroupItem> groups = _settledRespository.Records
                .GroupBy(x => x.Customer)
                .Select(g => new GroupItem()
                {
                    Customer = g.Key,
                    Wins = g.Select(x => x.Win).ToList()
                });

            var results = new List<int>();
            foreach (var gi in groups)
            {
                int total = gi.Wins.Count();
                int wins = total - gi.Wins.Count(x => x == 0);
                if (Decimal.Divide(wins, total) >= WIN_THRESHOLD)
                {
                    results.Add(gi.Customer);
                }
            }

            ViewBag.Title = "Settled > 60%";
            return View(results);
        }
    }
}
