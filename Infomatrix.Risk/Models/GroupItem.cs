﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomatrix.Risk.Models
{
    public class GroupItem
    {
        public int Customer { get; set; }
        public List<int> Wins { get; set; }
    }

}