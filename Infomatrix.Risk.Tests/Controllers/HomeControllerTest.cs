﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infomatrix.Risk;
using Infomatrix.Risk.Controllers;
using Infomatrix.Risk.Data;
using Moq;
using System.Collections.Generic;

namespace Infomatrix.Risk.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private static HomeController _controller;
        private static Mock<IRepository<Unsettled>> _unsettledRepo;
        private static Mock<IRepository<Settled>> _settledRepo;

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            _settledRepo = new Moq.Mock<IRepository<Settled>>();
            _unsettledRepo = new Moq.Mock<IRepository<Unsettled>>();
            _controller = new HomeController(_settledRepo.Object, _unsettledRepo.Object);
        }

        [TestMethod]
        public void test_customer_win_threshold()
        {
            //// Arrange
            _settledRepo.Setup(r => r.Records).Returns(new[] {
                new Settled() { Customer = 1, Win = 0 }, //lose
                new Settled() { Customer = 1, Win = 25 }, //Win
                new Settled() { Customer = 1, Win = 50 }, //Win
                new Settled() { Customer = 2, Win = 0 }, //Win
            });

            // Act
            ViewResult result = _controller.Settled() as ViewResult;
            var results = (List<int>)result.Model;

            // Assert
            Assert.IsTrue(results.Count == 1);
            Assert.AreEqual("Settled > 60%", result.ViewBag.Title);
        }
    }
}