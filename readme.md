# Informatrix test

#### Steps to run the project

1. Compile through Visual Studio to download nuget packages
2. Run the default project "Infomatrix.Risk"
    1. The default route is /Home/Settled which displays a list of customers with who have a win ratio of > 60%
3. Unit tests exist in "Infomatrix.Risk.Tests"