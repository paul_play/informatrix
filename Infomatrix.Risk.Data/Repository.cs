﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infomatrix.Risk.Data
{
    public interface IRepository<T>
    {
        IEnumerable<T> Records { get; }
    }

    public class Repository<T> : IRepository<T> where T : IEntity
    {
        private CsvReader _csvReader;

        public Repository(TextReader textReader)
        {
            _csvReader = new CsvReader(textReader);
        }

        public IEnumerable<T> Records
        {
            get
            {
                return _csvReader.GetRecords<T>();
            }
        }
    }
}
